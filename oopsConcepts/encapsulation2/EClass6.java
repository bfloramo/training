package encapsulation2;

import encapsulation1.EClass1;

public class EClass6 extends EClass1 {
	
	public static void main ( String [] args) {
		
		EClass6 ob6= new EClass6();
		
		System.out.println(ob6.text2);
		System.out.println(ob6.text3);
		ob6.method2();
		ob6.method3();
	}

}
