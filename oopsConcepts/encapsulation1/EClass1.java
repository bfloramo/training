package encapsulation1;

public class EClass1 {
	
	String text1="Default access modifier";
	public String text2="Public access modifier";
	protected String text3="Protected access modifier";
	private String text4="Private access modifier";
	
	
	void method1() {
		
		System.out.println("Default");
	}
	
	public void method2 () {
		System.out.println("public");
	}
	
	protected void method3() {
		System.out.println("protected");
	}
	
	private void method4() {
		System.out.println("private");
	}
	
	
	public static void main (String [] args) {
		
		EClass1 ob1= new EClass1();
		ob1.method1();
		ob1.method2();
		ob1.method3();
		ob1.method4();
		System.out.println(ob1.text1);
		System.out.println(ob1.text2);
		System.out.println(ob1.text3);
		System.out.println(ob1.text4);
	}
	
}
