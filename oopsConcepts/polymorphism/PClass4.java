package polymorphism;

public class PClass4 extends PClass3 {
	
	public static void main (String [] args) {
		
		PClass4 c= new PClass4();
		c.method2("this is selenium training");
		c.method2(5, 8);
		c.method2(10, "Allegis");
	}

}
