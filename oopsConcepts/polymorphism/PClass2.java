package polymorphism;

public class PClass2 extends PClass1 {
	//method overriding
	public void method1() {
		
		System.out.println("Executing Oveririding method 1");

	}
	
	public static void main (String [] args) {
		
		PClass2 c=new PClass2();
		c.method1();
	}

}
