package inheritance;

public class Grandson extends Son {
	
	public static void main (String [] args) {
		
		Grandson ob= new Grandson();
		ob.method1();
		ob.method2();
		System.out.println(ob.method3());
		ob.method4();
		ob.method5();
		
	}
	
	
	
	public void method5() {
		
		
		System.out.println("Executing method 5");
	}

}
