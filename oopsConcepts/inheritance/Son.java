package inheritance;

public class Son extends Father {
	


	public static void main (String [] args) {
		
		Son ob=new Son();
		ob.method4();
		ob.method1();
		ob.method2();
		System.out.println(ob.method3());
		System.out.println(ob.num);
		System.out.println(ob.text);
	}
	
	public void method4() {
		
		
		System.out.println("Executing method 4");
	}
	

	
}
