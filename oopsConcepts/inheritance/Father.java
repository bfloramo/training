package inheritance;

public class Father {
	
	int num=10;
	String text="Allegis";
	
	public void method1() {
		
		System.out.println("Executing method 1");
	}

	public void method2() {
		
		
		System.out.println("Executing method 2");
	}
	
	public int method3() {
		return 10;
	}
	
	public static void main (String [] args) {
		
		Father ob= new Father();
	
	}
}
