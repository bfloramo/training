package snowPages;

import com.framework.core.interfaces.impl.internal.ElementFactory;
import com.framework.utils.AllegisDriver;

public class Basepage {
	
	public AllegisDriver driver;
	
	public Basepage(AllegisDriver driver) {
		this.driver=driver;
		ElementFactory.initElements(driver, this);
		
	}

}
