package snowPages;

import java.util.ResourceBundle;

import org.openqa.selenium.support.FindBy;

import com.framework.core.interfaces.Button;
import com.framework.core.interfaces.Checkbox;
import com.framework.core.interfaces.Textbox;
import com.framework.utils.AllegisDriver;
import com.framework.utils.Constants;
import com.framework.utils.TestReporter;

public class LoginPage extends Basepage{

	public LoginPage(AllegisDriver driver) {
		super(driver);
		// TODO Auto-generated constructor stub
	}

	@FindBy(xpath="//input[@id='username']")
	private Textbox txtUserName;
	
	@FindBy(xpath="//input[@id='password']")
	private Textbox txtPassword;
	
	@FindBy(xpath="//input[@name='c.remember_me']")
	private Checkbox chkRememberMe;
	
	@FindBy(xpath="//input[@name='c.remember_me']")
	private Button btnLogin;
	
	public void loginWithCredentials(String username, String password) {
		txtUserName.set(username);
		TestReporter.log("Username entered");
		
		txtPassword.set(password);
		TestReporter.log("Password entered");
		
		chkRememberMe.click();	
		TestReporter.log("Uncheck Remember Me");
		
		btnLogin.click();
		TestReporter.log("Login Button clicked");
		
	}
	
	public void loginWithCredentialsENCRYPTED(String role) {
		
		ResourceBundle uc = ResourceBundle.getBundle(Constants.USER_CREDENTIALS_PATH);
		String username= uc.getString(role.toUpperCase());
		String password= uc.getString(role.toUpperCase()+"_PASSWORD");
		txtUserName.set(username);
		TestReporter.log("Username entered");
		
		txtPassword.set(password);
		TestReporter.log("Password entered");
		
		chkRememberMe.click();	
		TestReporter.log("Uncheck Remember Me");
		
		btnLogin.click();
		TestReporter.log("Login Button clicked");
		
	}
	
}
