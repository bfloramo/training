package annotations;

import org.testng.Assert;
import org.testng.annotations.Test;

public class FirstTest {
	
	@Test
	public void test1() {
		
		System.out.println("test 1");
	}
	
	@Test
	public void test2() {
		
		System.out.println("test 2 ");
	}
	
	@Test
	public void test3() {
		
		System.out.println("test 3");
	}
	
	@Test
	public void test4() {
		
		System.out.println("test 4");
	}
	
	@Test
	public void test5() {
		
		Assert.assertTrue(false);
		Assert.assertEquals(true, true);
	}
	

}
