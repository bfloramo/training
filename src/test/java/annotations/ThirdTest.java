package annotations;

import org.testng.annotations.Test;

public class ThirdTest {

	@Test(priority=0,enabled=true)
	public void test1() {
		
		System.out.println("test 1");
	}

	@Test(priority=1,enabled=false)
	public void test2() {
		
		System.out.println("test 2 ");
	}

	@Test(priority=2,enabled=true)
	public void test3() {
		
		System.out.println("test 3");
	}
	
	
	
}
