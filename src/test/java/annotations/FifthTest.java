package annotations;

import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class FifthTest {
	
	@BeforeClass
	public void bc() {
		System.out.println("datasetup");
	}
	
	@AfterClass
	public void ac() {
		System.out.println("datacleanup");
	}
	
	
	@BeforeMethod
	public void bm() {
		System.out.println("executing before the method");
	
	}
	
	@AfterMethod
	public void am() {
		System.out.println("executing After the method");

	}
	
	@Test
	public void test1() {

		System.out.println("test 1");

	}
	
	@Test
	public void test2() {
		
		System.out.println("test 2 ");
	}
	
	@Test
	public void test3() {
		
		System.out.println("test 3");
	}

}
