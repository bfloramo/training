package annotations;

import org.testng.Assert;
import org.testng.annotations.Test;

public class FourthTest {

	
	@Test(priority=0,enabled=true,description="this the test1")
	public void test1() {
		
		System.out.println("test 1");
	}

	@Test(priority=1,enabled=true)
	public void test2() {
		
		System.out.println("test 2 ");
		Assert.assertTrue(false);
	}

	@Test(priority=2,enabled=true, dependsOnMethods="test2")
	public void test3() {
		
		System.out.println("test 3");
	}
}
