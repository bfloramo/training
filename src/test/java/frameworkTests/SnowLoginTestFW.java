package frameworkTests;

import org.testng.annotations.Test;

import com.framework.utils.Preamble;
import com.framework.utils.TestEnvironment;

import snowPages.LoginPage;

public class SnowLoginTestFW extends TestEnvironment {
	
	@Test
	@Preamble(versionOneTestID= {"RT-03909"})
	public void loginTestSNOWFW() {
		setApplicationUnderTest("SERVICENOWPORTAL");
		testStart("Snow Login Test");
		
		LoginPage obj= new LoginPage(getDriver());
		//obj.loginWithCredentials("@Selenium NA End user General", "SNOW1234");
		obj.loginWithCredentialsENCRYPTED("SNOWLOGIN_TEST");
	}
	
	
	@Test
	@Preamble(versionOneTestID= {"RT-03909"})
	public void loginTestSNOWFW1() {
		setApplicationUnderTest("SERVICENOWPORTAL");
		testStart("Snow Login Test");
		
		LoginPage obj= new LoginPage(getDriver());
		obj.loginWithCredentials("@Selenium NA End user General", "SNOW1234");
		//obj.loginWithCredentialsENCRYPTED("SNOWLOGIN_TEST");
	}
	
	@Test
	@Preamble(versionOneTestID= {"RT-03909"})
	public void loginTestSNOWFW2() {
		setApplicationUnderTest("SERVICENOWPORTAL");
		testStart("Snow Login Test");
		
		LoginPage obj= new LoginPage(getDriver());
		obj.loginWithCredentials("@Selenium NA End user General", "SNOW123");
		//obj.loginWithCredentialsENCRYPTED("SNOWLOGIN_TEST");
	}
	

}
