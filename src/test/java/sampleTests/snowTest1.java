package sampleTests;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.Test;

public class snowTest1 {
	
	@Test
	public void loginTest() {
		
		// instance of the driver created
		WebDriver driver=new ChromeDriver();		
		String driverpath= System.getProperty("user.dir")+"\\chromedriver.exe";
		System.out.println(driverpath);
		System.setProperty("webdriver.chrome.driver", driverpath);	
		
		driver.get("https://myit.allegistest.com/sp");
		driver.manage().window().maximize();
		
		driver.findElement(By.xpath("//input[@id='username']")).click();
		driver.findElement(By.xpath("//input[@id='username']")).sendKeys("@Selenium NA End User General");
		
		
		driver.findElement(By.xpath("//input[@id='password']")).click();
		driver.findElement(By.xpath("//input[@id='password']")).sendKeys("SNOW1234");
		
		
		driver.findElement(By.xpath("//button[@name='login']")).click();
		
		
		WebElement waitObject= (new WebDriverWait(driver,60)).until(ExpectedConditions.presenceOfElementLocated(By.xpath("//div[@id='homepage-search']")));
		
		System.out.println(driver.findElement(By.xpath("//div[@id='homepage-search']")).isDisplayed());
		
		Assert.assertTrue(driver.findElement(By.xpath("//div[@id='homepage-search']")).isDisplayed());
		
		
		driver.findElement(By.xpath("//span[contains(text(),'@Selenium')]")).click();
		
		driver.findElement(By.xpath("//ul[@class='dropdown-menu']/li/a[contains(text(),'Logout')]")).click();
		
		driver.quit();
		
		
	}
	


}
