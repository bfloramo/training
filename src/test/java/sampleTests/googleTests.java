package sampleTests;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.Test;

public class googleTests {
	
	
	@Test
	public void test1() throws Exception {
		
		// instance of the driver created
		WebDriver driver=new ChromeDriver();		
		String driverpath= System.getProperty("user.dir")+"\\chromedriver.exe";
		System.out.println(driverpath);
		System.setProperty("webdriver.chrome.driver", driverpath);	
		
		//open google.com
		driver.get("https://www.google.com/");		
		driver.manage().window().maximize();	
	
		//perform assertion
		Assert.assertEquals(driver.getTitle(), "Google");		

		//print the current url
		System.out.println(driver.getCurrentUrl());
		
		WebElement googleSearchBox=driver.findElement(By.xpath("//input[@name='q']"));
		
		googleSearchBox.click();
		googleSearchBox.sendKeys("Allegis");
		googleSearchBox.submit();
		

		

		//hard wait // use implicit wait/explicit wait
		//Thread.sleep(10000);
		
		WebElement waitObject= (new WebDriverWait(driver,60)).until(ExpectedConditions.presenceOfElementLocated(By.xpath("//div[@id='result-stats']")));
		
		//check whether page loaded or not
		
		boolean resultsflag=driver.findElement(By.xpath("//div[@id='result-stats']")).isDisplayed();
		System.out.println(resultsflag);
		
		/*
		driver.findElements(By.xpath("//div[@class='r']/a")).get(0).getText();
		System.out.println(driver.findElements(By.xpath("//div[@class='r']/a")).get(2).getText());
		System.out.println(driver.findElements(By.xpath("//div[@class='r']/a")).get(3).getText());
		*/
		
		
		List<WebElement> allLinks=driver.findElements(By.xpath("//div[@class='r']/a"));
		
		System.out.println(allLinks.size());
		
		for (int i=0;i<allLinks.size();i++) {
			
			String searchedlinks= allLinks.get(i).getText();
			System.out.println("Searched Results: "+searchedlinks);
			Thread.sleep(3000);
			
		}
		
		
		
		
		//close driver( which will close the browser)
		driver.close();

	}

}
