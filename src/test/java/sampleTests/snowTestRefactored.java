package sampleTests;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class snowTestRefactored {
	
	public WebDriver driver=null;
	
	@BeforeMethod
	public void driverSetup() {
		// instance of the driver created
		driver=new ChromeDriver();		
		String driverpath= System.getProperty("user.dir")+"\\chromedriver.exe";
		System.out.println(driverpath);
		System.setProperty("webdriver.chrome.driver", driverpath);	
		
	}
	
	@AfterMethod
	public void closeBrowser() {
		driver.quit();
	}
	
	
	@Test
	public void loginTest() {
		
		String url="https://myit.allegistest.com/sp";
		String uName="@Selenium NA End User General";
		String pwd="SNOW1234";
		
		
		driver.get(url);
		driver.manage().window().maximize();
		
		driver.findElement(By.xpath("//input[@id='username']")).click();
		driver.findElement(By.xpath("//input[@id='username']")).sendKeys(uName);
		
		
		driver.findElement(By.xpath("//input[@id='password']")).click();
		driver.findElement(By.xpath("//input[@id='password']")).sendKeys(pwd);
		
		
		driver.findElement(By.xpath("//button[@name='login']")).click();
		
		
		WebElement waitObject= (new WebDriverWait(driver,60)).until(ExpectedConditions.presenceOfElementLocated(By.xpath("//div[@id='homepage-search']")));
		
		System.out.println(driver.findElement(By.xpath("//div[@id='homepage-search']")).isDisplayed());
		
		Assert.assertTrue(driver.findElement(By.xpath("//div[@id='homepage-search']")).isDisplayed());
		
		
		driver.findElement(By.xpath("//span[contains(text(),'@Selenium')]")).click();
		
		driver.findElement(By.xpath("//ul[@class='dropdown-menu']/li/a[contains(text(),'Logout')]")).click();
		
	
	
	}
	
}
