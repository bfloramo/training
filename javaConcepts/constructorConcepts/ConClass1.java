package constructorConcepts;

public class ConClass1 {
	
	
	// Following is the normal constructor and it will have the same name as the class name, block of code similar to the method and its called when the instance is created
	
	public ConClass1 () { 
		
		System.out.println("This is a constructor");
		
		System.out.println("Execute Pre reqs");
	
	}
	
	public static void main(String[] args) {

		ConClass1 ob= new ConClass1(); // When an object is created, the constructor is called 

	}

}
