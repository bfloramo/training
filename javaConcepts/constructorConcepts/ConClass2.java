package constructorConcepts;

public class ConClass2 {
	
	// Following is a normal constructor and it will have the same name as the class name

	
	public ConClass2() { 
		
		System.out.println("This is a normal Constructor");
	
	}
	
	// Following is a parameterized constructor and it will have the same name as the class name
	
	public ConClass2(String text) { 
		
		System.out.println(text);
	
	}
	
	public ConClass2(int a, int b) { 
		
		System.out.println("sum:"+(a+b));
	
	}
	
	public static void main(String[] args) {
		

		ConClass2 ob= new ConClass2(); 
		ConClass2 ob1= new ConClass2("This is a parameterized Constructor");
		ConClass2 ob2= new ConClass2(10,2);

	}
}
