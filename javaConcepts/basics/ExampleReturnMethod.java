package basics;

public class ExampleReturnMethod {
		
	//method which returns a value should declare the data type of the value it returns.
	public String methodReturn() {
		
		return "executing return method";
		
	}

}
