package basics;

public class NonStaticExample {
	
	public static void main (String [] args) {
		
		// Create an instance of the class and ob1 is the object created to access the methods int the same class
		NonStaticExample ob1= new NonStaticExample();
		ob1.method1();
		ob1.method2();
		System.out.println(ob1.method3());
		
		//Create an instance of the class where you want to access the methods and ob2 is the object created to access the methods
		NonStaticMethods ob2= new NonStaticMethods();
		ob2.method4();
		ob2.method5();
		System.out.println(ob2.method6());
				
	}
		

	// Non static method
	public void method1() {
		
		//code
		//code
		System.out.println("Executing method 1");
	}

	// Non static method
	public void method2() {
		
		//code
		//code
		System.out.println("Executing method 2");
	}
	
	// Non static method
	public String method3() {
		
		//code
		//code
		return "Executing method 3 with return type";
	}
}




