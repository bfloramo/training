package basics;

public class StaticMethods {
	
	
	// Static method
	public static void method10() {
		
		//code
		//code
		//code
		System.out.println("Executing Static method 10");
	}

	// Static method
	public static void method11() {
		
		//code
		//code
		//code
		System.out.println("Executing Static method 11");
	}
	
	// Static method
	public static String method12() {
		
		//code
		//code
		//code
		return "Executing Static method 12 with return type";
	}
}

