package basics;

public class MethodsArguments {
	
	public static void main (String [] args) {
		
		//accessing the static method and passing the arguments values
		sum(5,4);
		
	}
		
	
	//Creating a Simple method with two arguments
	public static void sum(int a, int b) {
		
		int c= a+b;
		System.out.println("Sum:"+c);
	}

}
