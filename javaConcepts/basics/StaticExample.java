package basics;

public class StaticExample {
	
	public static void main (String [] args) {
		
		//for static methods-no need to create the instance of the class or create an objects, methods can be acessed directlly
		method7();
		method8();
		System.out.println(method9());
		
		//for static methods from another class- no need to create the instance just call the methods with className.methodName
		StaticMethods.method10();
		StaticMethods.method11();
		System.out.println(StaticMethods.method12());		
	}
	


	// Static method
	public static void method7() {
		
		//code
		//code
		System.out.println("Executing Static method 7");
	}

	// Static method
	public static void method8() {
		
		//code
		//code
		System.out.println("Executing Static method 8");
	}
	
	// Static method
	public static String method9() {
		
		//code
		//code
		return "Executing Static method 9 with return type";
	}
}
