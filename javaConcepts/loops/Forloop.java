package loops;

public class Forloop {
	
	//simple for loop
	public void printNumbers(int number) {
		
		for( int i=1;i<=number;i++) {
			
			System.out.println(i);
		}
		
	}
	
	public static void main ( String [] args) {
		
		Forloop ob= new Forloop();
		ob.printNumbers(3);


	}

}
